package ru.vartanyan.tm.service;

import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.api.IService;
import ru.vartanyan.tm.api.repository.ITaskRepository;
import ru.vartanyan.tm.model.AbstractEntity;

import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    private IRepository<E> repository;

    public AbstractService(IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public E add(E entity) {
        if (entity == null) return null;
        return repository.add(entity);
    }

    @Override
    public E findById(String id) {
        if (id == null || id.isEmpty()) return null;
        return repository.findById(id);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public void removeById(String id) {
        if (id == null || id.isEmpty()) return;
        repository.removeById(id);
    }

    @Override
    public void remove(E entity) {
        if (entity == null) return;
        repository.remove(entity);
    }

}
