package ru.vartanyan.tm.api.service;

import ru.vartanyan.tm.api.IService;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.model.User;

import java.util.List;

public interface IUserService extends IService<User> {

    User findByLogin(final String login) throws Exception;

    void removeByLogin(final String login) throws Exception;

    User create(final String login,
                final String password) throws Exception;

    User create(final String login,
                final String password,
                final String email) throws Exception;

    User create(final String login,
                final String password,
                final Role role) throws Exception;

    User setPassword(final String userId, final String password) throws Exception;

    User updateUser(final String userId,
                    final String firstName,
                    final String lastName,
                    final String middleName) throws Exception;

}
