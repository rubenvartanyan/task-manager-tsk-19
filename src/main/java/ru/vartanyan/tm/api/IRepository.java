package ru.vartanyan.tm.api;

import ru.vartanyan.tm.model.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    List<E> findAll();

    E add(final E entity);

    E findById(final String id);

    void clear();

    void removeById(final String id);

    void remove(final E entity);

}
