package ru.vartanyan.tm.api.repository;

import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public interface IUserRepository extends IRepository<User> {

    public User findByLogin(final String login);

    public void removeByLogin(final String login);

}
