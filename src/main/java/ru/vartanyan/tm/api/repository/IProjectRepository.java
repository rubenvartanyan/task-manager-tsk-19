package ru.vartanyan.tm.api.repository;

import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IRepository<Project>{

    List<Project> findAll(Comparator<Project> comparator);

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    void removeOneByIndex(Integer index);

    void removeOneByName(String name);

}
