package ru.vartanyan.tm.api.repository;

import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    List<Task> findAll(Comparator<Task> comparator);

    List<Task> finaAllByProjectId(String projectId);

    List<Task> removeAllByProjectId(String projectId);

    Task bindTaskByProjectId(String projectId, String taskId);

    Task unbindTaskFromProject(String projectId, String taskId);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    void removeOneByIndex(Integer index);

    Task removeOneByName(String name);

}
