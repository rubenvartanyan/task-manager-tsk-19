package ru.vartanyan.tm.repository;

import ru.vartanyan.tm.api.repository.ITaskRepository;
import ru.vartanyan.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAll(Comparator<Task> comparator) {
        final List<Task> tasks = new ArrayList<>(entities);
        tasks.sort(comparator);
        return tasks;
    }

    @Override
    public List<Task> finaAllByProjectId(String projectId) {
        final List<Task> listOfTask = new ArrayList<>();
        for (Task task: entities){
            if (task.getProjectId().equals(projectId)) listOfTask.add(task);
        }
        return listOfTask;
    }

    @Override
    public List<Task> removeAllByProjectId(String projectId) {
        for (Task task: entities) {
            if (task.getProjectId().equals(projectId)) task.setProjectId("");
        }
        return entities;
    }

    @Override
    public Task bindTaskByProjectId(String projectId, String taskId) {
        final Task task = findById(taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskFromProject(String projectId, String taskId) {
        final Task task = findById(taskId);
        task.setProjectId(null);
        return task;
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        return entities.get(index);
    }

    @Override
    public Task findOneByName(final String name) {
        for (Task task: entities) {
            if (task.getName().equals(name)) return task;
        }
        return null;
    }

    @Override
    public void removeOneByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        entities.remove(task);
    }

    @Override
    public Task removeOneByName(final String name) {
        final Task task = findOneByName(name);
        entities.remove(task);
        return task;
    }

}
