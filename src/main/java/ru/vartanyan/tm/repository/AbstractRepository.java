package ru.vartanyan.tm.repository;

import ru.vartanyan.tm.api.IRepository;
import ru.vartanyan.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> entities = new ArrayList<>();

    @Override
    public List<E> findAll() {
        return entities;
    }

    @Override
    public E add(final E entity) {
        entities.add(entity);
        return entity;
    }

    @Override
    public E findById(final String id) {
        if (id == null || id.isEmpty()) return null;
        for (final E entity: entities) {
            if (entity == null) continue;
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public void clear() {
        entities.clear();
    }

    @Override
    public void removeById(final String id) {
        final E entity = findById(id);
        if (entity == null) return;
        entities.remove(entity);
    }

    @Override
    public void remove(final E entity) {
        entities.remove(entity);
    }

}
