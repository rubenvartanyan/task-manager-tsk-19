package ru.vartanyan.tm.repository;

import ru.vartanyan.tm.api.repository.IProjectRepository;
import ru.vartanyan.tm.model.AbstractEntity;
import ru.vartanyan.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public List<Project> findAll(Comparator<Project> comparator) {
        final List<Project> projects = new ArrayList<>(entities);
        projects.sort(comparator);
        return projects;
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        return entities.get(index);
    }

    @Override
    public Project findOneByName(final String name) {
        for (Project project: entities) {
            if (project.getName().equals(name)) return project;
        }
        return null;
    }

    @Override
    public void removeOneByIndex(final Integer index) {
        final Project project = findOneByIndex(index);
        entities.remove(project);
    }

    @Override
    public void removeOneByName(final String name) {
        final Project project = findOneByName(name);
        entities.remove(project);
    }

}
