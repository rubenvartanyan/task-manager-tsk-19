package ru.vartanyan.tm.repository;

import ru.vartanyan.tm.api.repository.IUserRepository;
import ru.vartanyan.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(String login) {
        for (final User user: entities) {
            if (user.getLogin().equals(login)) return user;
        }
        return null;
    }

    @Override
    public void removeByLogin(String login) {
        final User user = findByLogin(login);
        remove(user);
    }

}
